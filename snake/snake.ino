#include "LedControl.h"
#include "pitches.h"
#include <LiquidCrystal.h>
#define joyX A0
#define joyY A1

int foodSound = NOTE_B7; // used many times


// Important definitions of the LCD screen and LED matrices

const int rs = 3, en = 4, d4 = 10, d5 = 5, d6 = 6, d7 = 7; 
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
LedControl lc = LedControl(11, 13, 9, 4);


// set any pixel on the 32x8 matrix grid on or off - very important function

void setPixel(int x, int y, char state) { 
  lc.setLed(x/8, x%8, y, state);
}


// creates coordinates for random food generation - snake position is factored in later

int createFoodPositionX() { 
  int seedX = micros();
  seedX = seedX * 1103515245 + 12345;
  seedX = seedX % 32;
  return abs(seedX);
}
int createFoodPositionY() {
  int seedY = micros();
  seedY = seedY * 1103515245 + 12345;
  seedY = seedY % 8;
  return abs(seedY);
}


// turns on the LCD screen, LED matrices 

void setup() {
  lcd.begin(16, 2);
  for (int i=0; i<4;i++) {
    lc.shutdown(i, false);
    lc.setIntensity(i, 1);
    lc.clearDisplay(i); 
  } 
  tone(8, foodSound, 250);
  delay(325);
  noTone(8);
  lcd.print("Welcome!");
  delay(1000);
}


// High score variables - exist as long as the system is running

int easyHighScore = 0;
int medHighScore = 0;
int hardHighScore = 0;


// Main loop of the game

void loop() {

  
  // Variable initializations - reset every round
  
  int tickSpeed;
  String velocity = "right";
  int snakeSize = 3;
  bool food = false; // has the snake consumed food this frame?
  bool foodOnBoard = false; // is there food on the board?
  
  int food_coords[2] = {10, 5}; // used to store the position of the food throughout the round
  
  int snake[128][2] = { // holds all occupied positions of the snake
    {17, 3},
    {16, 3},
    {15, 3}
  };

  bool start = false; // variables for the difficulty selector loop
  String difficulty = "Medium";


  // difficulty selector loop
  
  while (start == false) { 
    int xValue = analogRead(joyX);
    int yValue = analogRead(joyY);


    // Joystick X position is used to switch between difficulties, making noise with every change
    
    if (difficulty == "Easy") {
      if (xValue < 200) {
        difficulty = "Medium";
        tone(8, foodSound, 250);
        delay(325);
        noTone(8);
      }
    } else if (difficulty == "Medium") {
      if (xValue > 800) {
        difficulty = "Easy";
        tone(8, foodSound, 250);
        delay(325);
        noTone(8);
      } else if (xValue < 200) {
        difficulty = "Hard";
        tone(8, foodSound, 250);
        delay(325);
        noTone(8);
      } else {
        ;
      }
    } else if (difficulty == "Hard") {
      if (xValue > 800) {
        difficulty = "Medium";
        tone(8, foodSound, 250);
        delay(325);
        noTone(8);
      }
    }

    // Displays the difficulty as well as instructions to start the game
    delay(100);
    lcd.clear();
    lcd.print(difficulty);
    lcd.setCursor(0, 1);
    lcd.print("Move stick down");
    if (yValue > 800) {
      start = true;
    }   
  }


  // Uses the difficulty variable to set the speed of the game
  
  if (difficulty == "Easy") {
    tickSpeed = 500;
  } else if (difficulty == "Medium") {
    tickSpeed = 250;
  } else {
    tickSpeed = 125;
  }


  // Starting melody
  
  tone(8, NOTE_A3, 500);
  delay(600);
  noTone(8);
  tone(8, NOTE_C4, 500);
  delay(600);
  noTone(8);

  
  // Main loop of the game - one round
  
  bool gameOver = false; //
  while (gameOver == false) {

    
    // Uses joystick position to adjust the direction of the snake

    int xValue = analogRead(joyX);
    int yValue = analogRead(joyY);
    if ((yValue > 800) && (100 < xValue < 900) && (velocity != "down")) {
      velocity = "up";
    } else if ((yValue < 200) && (100 < xValue < 900) && (velocity != "up")) {
      velocity = "down";
    } else if ((xValue > 800) && (100 < yValue < 900) && (velocity != "left")) {
      velocity = "right";
    } else if ((xValue < 200) && (100 < yValue < 900) && (velocity != "right")) {
      velocity = "left";
    } else {
      ;
    }

  
   // Creates new snake data based on existing coordinates and direction
    
    int new_snake[128][2] = {};
  
    if (velocity == "right") {
      new_snake[0][0] = (snake[0][0])+1; // first x coordinate in the newest snake is the first one shifted right
      new_snake[0][1] = snake[0][1]; // first y coordinate is unchanged
    } else if (velocity == "left") {
      new_snake[0][0] = (snake[0][0])-1; // first x coordinate in the new snake is shifted left
      new_snake[0][1] = snake[0][1]; // unchanged
    } else if (velocity == "up") {
      new_snake[0][0] = snake[0][0];
      new_snake[0][1] = snake[0][1]+1;
    } else if (velocity == "down") {
      new_snake[0][0] = snake[0][0];
      new_snake[0][1] = snake[0][1]-1;
    } else {
      ;
    }


    // generates new food onto the board if earlier food has been eaten
    
    if (foodOnBoard == false) { 
      food_coords[0] = createFoodPositionX();
      delay(1);
      food_coords[1] = createFoodPositionY();


      // If the food coordinates place it inside the snake, generates new coordinates

      bool snake_match = false;
      for (int i=0; i < snakeSize;i++) { // iterates through the snake positions to find a match
        if ((food_coords[0] == snake[i][0]) && (food_coords[1] == snake[i][1])) {
          snake_match = true;
        }
      }


      // randomly generates new coords until they do not match any snake position
      
      while (snake_match == true) { 
        bool snake_match_ii = false; // used to close the loop
        food_coords[0] = createFoodPositionX();
        delay(1);
        food_coords[1] = createFoodPositionY(); // creates new position
      
        for (int i=0; i < snakeSize; i++) { // iterates through the snake positions to find a match
          if ((food_coords[0] == snake[i][0]) && (food_coords[1] == snake[i][1])) {
            snake_match_ii = true; // keeps the thing running
          }
        }
        if (snake_match_ii == false) { // if no matches anywhere, close the loop
          snake_match = false; 
        }
      }
      
      foodOnBoard = true;
    }


    // If the snake is touching food, adjusts the food variable
    
    if ((snake[0][0] == food_coords[0]) && (snake[0][1] == food_coords[1])) {
      food = true;
    } else {
      food = false;
    }


    // Uses the food variable to either maintain a constant snake length or increase it by 1
    
    if (!food) { // If the snake has not consumed food, move the back position to the front, maintaining a constant length
      for (int i=1; i < snakeSize; i++) {
        new_snake[i][0] = snake[i-1][0];
        new_snake[i][1] = snake[i-1][1];
      }
    } else { // If the snake has consumed food, keep the last space, increasing the length of your snake by 1
      for (int i=1; i <= snakeSize; i++) {
        new_snake[i][0] = snake[i-1][0];
        new_snake[i][1] = snake[i-1][1];
      }
      snakeSize++; // increases the length of the snake in memory
    }


    // Updates the snake data with the new_snake data
    
    snake[128][2] = {};
    for (int x=0; x < snakeSize; x++) {
      for (int y=0; y < 2; y++) {
        snake[x][y] = new_snake[x][y];
      }
    }


    // Clears the screen
    
    for (int row=0; row <8;row++) {
      for (int x = 0; x < 32;x++) {
        setPixel(x, row, false);
      }
    }


    // Prints the snake and food to the board
    
    for (int i=0; i < snakeSize; i++) {
      setPixel(snake[i][0], snake[i][1], true);
    }
    setPixel(food_coords[0], food_coords[1], true);


    // Implements the tickSpeed delay and makes a sound if the snake has consumed food
    if (food == true) {
      tone(8, foodSound, 250);
      delay(tickSpeed);
      noTone(8);
      foodOnBoard = false;
      food = false;
    } else {
      delay(tickSpeed);
    }


    // Checks for deaths
    
    if ((snake[0][0] > 31) || (snake[0][0] < 0) || (snake[0][1] > 7) || (snake[0][1] < 0)) { // Ends the game if the snake goes outside the map
      gameOver = true;
    }
    for (int i= 1; i < snakeSize;i++) { // Ends the game if the snake runs into itself
      if ((snake[i][0] == snake[0][0]) && (snake[i][1] == snake[0][1])) {
        gameOver = true;
      }
    }
    
    lcd.clear();
    lcd.print(snakeSize - 3); // Informs the player of their score
  }


  // Plays a sound when the round ends
  tone(8, NOTE_D3, 250);
      delay(325);
      noTone(8);
      tone(8, NOTE_D2, 250);
      delay(325);
      noTone(8);
  
  // updates high score if it has been broken
  if ((difficulty == "Medium") && ((snakeSize-3) > medHighScore)) {
    medHighScore = snakeSize - 3;
  } else if ((difficulty == "Hard") && ((snakeSize-3) > hardHighScore)) {
    hardHighScore = snakeSize - 3;
  } else if ((difficulty == "Easy") && ((snakeSize-3) > easyHighScore)) {
    easyHighScore = snakeSize - 3;
  } else {
    ;
  }

  bool restart = false;
  while (restart == false) {
    int i = 0;
    while (i < 60) { // 6 seconds for each cycle
      lcd.clear();
      if (i < 30) { // 3 seconds for each part
        lcd.print("Your Score: ");
        lcd.print(snakeSize - 3);
        lcd.setCursor(0, 1);

        if (difficulty == "Easy") {
          lcd.print("Easy Record: ");
          lcd.print(easyHighScore);
        } else if (difficulty == "Medium") {
          lcd.print("Med Record: ");
          lcd.print(medHighScore);
        } else if (difficulty == "Hard") {
          lcd.print("Hard Record: ");
          lcd.print(hardHighScore);
        } else {
          ;
        }
        
      } else {
        lcd.print("Move joystick");
        lcd.setCursor(0, 1);
        lcd.print("to play again");
      }
      int xValue = analogRead(joyX);
      int yValue = analogRead(joyY);
      if ((xValue < 400) || (xValue > 600) || (yValue < 400) || (yValue > 600)) {
        restart = true;
        i = 100;
      }
      delay(100);
      i++;
    }
  }
  tone(8, foodSound, 250);
  delay(325);
  noTone(8);
  delay(1675);
}
